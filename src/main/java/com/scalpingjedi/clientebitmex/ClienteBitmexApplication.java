package com.scalpingjedi.clientebitmex;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import com.scalpingjedi.clientebitmex.servicio.user.UserService;

@SpringBootApplication
public class ClienteBitmexApplication implements CommandLineRunner {

	Logger log = LoggerFactory.getLogger(ClienteBitmexApplication.class);

	@Value("${BITMEX.APIKEY}")
	private String apiKey;

	@Value("${BITMEX.APISECRET}")
	private String apiSecret;

	public static void main(String[] args) {
		SpringApplication.run(ClienteBitmexApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		ejemploCuentaUsuario();

		ejemploWalletUsuario();

		ejemploExtraccion();

		ejemploConfirmacionExtraccion();

	}

	private void ejemploCuentaUsuario() {
		System.out.println("<<<< EJEMPLO CUENTA DE USUARIO >>>>");
		UserService userService = new UserService(apiSecret, apiKey);
		ResponseEntity<String> respuesta = userService.infoUsuario();
		System.out.println("Respuesta: " + respuesta.getBody());
		System.out.println();
	}

	private void ejemploWalletUsuario() {
		System.out.println("<<<< EJEMPLO WALLET DE USUARIO >>>>");
		UserService userService = new UserService(apiSecret, apiKey);
		ResponseEntity<String> respuesta = userService.wallet("XBt");
		System.out.println("Respuesta: " + respuesta.getBody());
		System.out.println();
	}

	private void ejemploExtraccion() {
		System.out.println("<<<< EJEMPLO DE PETICION DE EXTRACCION >>>>");
		UserService userService = new UserService(apiSecret, apiKey);
		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

				System.out.print("Introduce el token OTP (2Factor Google):");
				String otp = reader.readLine();

//			System.out.print("Introduce la divisa (ejemplo=>XBt):");
//			String currency = reader.readLine();
				String currency = "XBt";

//			System.out.print("Introduce la cantidad:");
//			String amount = reader.readLine();
				Double amount = 0.012d;
//				BigDecimal amount = new BigDecimal("0.009");

//			System.out.print("La Wallet de destino:");
//			String addressDestination = reader.readLine();
				String addressDestination = "37mVbR54UwCHmwMPAojVbdfD5sUNM7Mree";

				ResponseEntity<String> respuesta = userService.requestWithdrawal(otp, currency, amount, addressDestination);
				System.out.println("Respuesta: " + respuesta.getBody());
				if (respuesta.getStatusCodeValue() == 200) {
					break;
				}
			} catch (Exception e) {
				String mensajeError = ((HttpClientErrorException) e).getResponseBodyAsString();
				System.out.println(mensajeError);
			}
		}
		System.out.println();
	}

	private void ejemploConfirmacionExtraccion() {
		System.out.println("<<<< EJEMPLO DE CONFIRMACION EXTRACCION >>>>");
		UserService userService = new UserService(apiSecret, apiKey);
		while (true) {
			try {
//			Enter data using BufferReader 
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

				System.out.print("Introduce el token de confirmación (recibido por email):");
				String tokenConfirmation = reader.readLine();
				ResponseEntity<String> respuesta = userService.confirmWithdrawal(tokenConfirmation);
				System.out.println("Respuesta: " + respuesta.getBody());
				if (respuesta.getStatusCodeValue() == 200) {
					break;
				}
			} catch (Exception e) {
				String mensajeError = ((HttpClientErrorException) e).getResponseBodyAsString();
				System.out.println(mensajeError);
			}
		}
		System.out.println();
	}
}
