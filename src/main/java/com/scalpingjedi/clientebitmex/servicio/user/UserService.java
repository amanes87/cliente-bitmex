package com.scalpingjedi.clientebitmex.servicio.user;

import java.math.BigDecimal;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.scalpingjedi.clientebitmex.servicio.BitmexSv;

public class UserService extends BitmexSv{

	
	public UserService(String apiSecret, String apiKey) {
		super(apiSecret, apiKey);
	}

	/**
	 * Método encargado de devolver información de la cuenta de usuario.
	 * @return
	 */
	public ResponseEntity<String> infoUsuario(){
		String path = "/user";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", construirHeaders(path, HttpMethod.GET.name(), ""));
		ResponseEntity<String> result = restTemplate.exchange(URL + path, HttpMethod.GET, entity, String.class);
		return result;
	}
	
	/**
	 * Método enecargado de devolver información sobre una wallet determinada.
	 * @param currency
	 * @return
	 */
	public ResponseEntity<String> wallet(String currency){
		String path = "/user/wallet?currency="+currency;
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", construirHeaders(path, HttpMethod.GET.name(), ""));
		ResponseEntity<String> result = restTemplate.exchange(URL + path, HttpMethod.GET, entity, String.class);
		return result;
	}
	
	/**
	 * Método que solicita una extracción de dinero de la cuenta de Bitmex a una wallet que se pase por parámetro.
	 * @param otp
	 * @param currency
	 * @param amount
	 * @param addressDestination
	 * @return
	 */
	public ResponseEntity<String> requestWithdrawal(String otp, String currency, Double amount, String addressDestination){
		String path = "/user/requestWithdrawal";
		String data = "otpToken="+otp+"&currency="+currency+"&amount="+amount.doubleValue()+"&address="+addressDestination;
		RestTemplate restTemplate = new RestTemplate();
		
		MultiValueMap<String, Object> map =new LinkedMultiValueMap<String, Object>();
		map.add("otpToken",otp);
		map.add("currency",currency);
		map.add("amount", amount.doubleValue());
		map.add("address",addressDestination);
		
		HttpHeaders headers = construirHeaders(path, HttpMethod.POST.name(), data);
		headers.add("Content-Type", "application/x-www-form-urlencoded");

		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
		ResponseEntity<String> result = restTemplate.exchange(URL + path, HttpMethod.POST, entity, String.class);
		return result;
	}
	
	/**
	 * Método que se encarga de confirmar una extracción de dinero.
	 * @param tokenConfirmation Se recibe por email.
	 * @return
	 */
	public ResponseEntity<String> confirmWithdrawal(String tokenConfirmation){
		String path = "/user/confirmWithdrawal";
		String data = "token="+tokenConfirmation;
		RestTemplate restTemplate = new RestTemplate();
		
		MultiValueMap<String, Object> map =new LinkedMultiValueMap<String, Object>();
		map.add("token",tokenConfirmation);
		
		HttpHeaders headers = construirHeaders(path, HttpMethod.POST.name(), data);
		headers.add("Content-Type", "application/x-www-form-urlencoded");

		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
		ResponseEntity<String> result = restTemplate.exchange(URL + path, HttpMethod.POST, entity, String.class);
		return result;
	}
}
