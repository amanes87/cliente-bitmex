package com.scalpingjedi.clientebitmex.servicio;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Hex;
import org.springframework.http.HttpHeaders;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BitmexSv {
	
	private String apiSecret;
	
	private String apiKey;
	
	private final String URLBASE = "https://www.bitmex.com";
	
	private final String URLVERSIONAPI = "/api/v1";
	
	protected final String URL = URLBASE + URLVERSIONAPI;
	
	public BitmexSv(String apiSecret, String apiKey) {
		this.apiSecret = apiSecret;
		this.apiKey = apiKey;
	}
	
	/**
	 * Método común para inicializar los headers de la petición.
	 * @param pathRuta
	 * @param verbo
	 * @param data
	 * @return
	 */
	protected HttpHeaders construirHeaders(String pathRuta, String verbo, String data) {
		String expires = String.valueOf(System.currentTimeMillis() + 3000000L);
		HttpHeaders headers = new HttpHeaders();
		headers.add("api-key", apiKey);
		headers.add("api-signature", obtenerSignature(pathRuta, verbo, expires, data));
		headers.add("api-expires", expires);
		headers.add("Accept", MediaType.APPLICATION_JSON);
		return headers;
	}

	/**
	 * Método encargado de construir la signature de la petición.
	 * @param pathRuta
	 * @param verbo
	 * @param expires
	 * @param data
	 * @return
	 */
	protected String obtenerSignature(String pathRuta, String verbo, String expires, String data) {

		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(apiSecret.getBytes("UTF-8"), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			String path = URLVERSIONAPI + pathRuta;
			String valorAEncriptar = verbo + path + expires + data;
			String signature = Hex.encodeHexString(sha256_HMAC.doFinal((valorAEncriptar).getBytes("UTF-8")));
			return signature;
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
}
