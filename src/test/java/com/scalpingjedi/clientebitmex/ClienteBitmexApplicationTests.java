package com.scalpingjedi.clientebitmex;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ClienteBitmexApplicationTests {
	
	Logger log = LoggerFactory.getLogger(ClienteBitmexApplicationTests.class);

	private String URLBASE = "https://www.bitmex.com/api/v1";

	@Test
	public void contextLoads() {
	}

	@Test
	public void pruebaAutenticacion() {
		String apiSecret = "I1zrSLpH9doQO24EYlsusC2Cn5fi56kGtghL7U5pyJWe7M_H";
		String apiKey = "8tg15iacfwNuimpmM-hZQPos";
		String expires = String.valueOf(System.currentTimeMillis() + 3000000L);

		RestTemplate restTemplate = new RestTemplate();
		String path = "/user";
		String signature = obtenerSignature(apiSecret, path, HttpMethod.GET.name(), expires, "");
		HttpHeaders headers = new HttpHeaders();
		headers.add("api-key", apiKey);
		headers.add("api-signature", signature);
		headers.add("api-expires", expires);
		headers.add("Accept", MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(URLBASE + path, HttpMethod.GET, entity, String.class);

		System.out.println("RESPUESTA " + result.getBody());
	}
	
	@Test
	public void pruebaWallet() {
		String apiSecret = "I1zrSLpH9doQO24EYlsusC2Cn5fi56kGtghL7U5pyJWe7M_H";
		String apiKey = "8tg15iacfwNuimpmM-hZQPos";
		String expires = String.valueOf(System.currentTimeMillis() + 3000000L);

		RestTemplate restTemplate = new RestTemplate();
		String path = "/user/wallet?currency=XBt";
//		String data = "{\"currency\":\"XBt\"}";
		String data = "";
		String signature = obtenerSignature(apiSecret, path, HttpMethod.GET.name(), expires, data);
		HttpHeaders headers = new HttpHeaders();
		headers.add("api-key", apiKey);
		headers.add("api-signature", signature);
		headers.add("api-expires", expires);
		headers.add("Accept", MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(URLBASE + path, HttpMethod.GET, entity, String.class);

		System.out.println("RESPUESTA " + result.getBody());
	}

	@Test
	public void pruebaRequestWithDraw() {
		String apiKey = "8tg15iacfwNuimpmM-hZQPos";
		String apiSecret = "I1zrSLpH9doQO24EYlsusC2Cn5fi56kGtghL7U5pyJWe7M_H";
		String expires = String.valueOf(System.currentTimeMillis() + 3000000L);

		RestTemplate restTemplate = new RestTemplate();

		String otp = "361524";
//		String data = "otpToken="+otp+"&currency=XBt&amount=0,001&address=37mVbR54UwCHmwMPAojVbdfD5sUNM7Mree";
		String data = "{\"otpToken\":\""+otp+"\",\"currency\":\"XBt\",\"amount\":0.001,\"address\":\"37mVbR54UwCHmwMPAojVbdfD5sUNM7Mree\"}";
		String verbo = HttpMethod.POST.name();
		String path = "/user/requestWithdrawal";
		String signature = obtenerSignature(apiSecret, path, verbo, expires, data);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("api-key", apiKey);
		headers.add("api-signature", signature);
		headers.add("api-expires", expires);
		headers.add("Accept", MediaType.APPLICATION_JSON);
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		
		MultiValueMap<String, String> map =new LinkedMultiValueMap<String, String>();
		map.add("otpToken",otp);
		map.add("currency","XBt");
		map.add("amount","0.001");
		map.add("address","37mVbR54UwCHmwMPAojVbdfD5sUNM7Mree");
		
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		try {
			ResponseEntity<String> result = restTemplate.exchange(URLBASE + path, HttpMethod.POST, entity, String.class);
			System.out.println("RESPUESTA " + result.getBody());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String obtenerSignature(String apiSecret, String pathRuta, String verbo, String expires, String data) {

		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(apiSecret.getBytes("UTF-8"), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			String path = "/api/v1" + pathRuta;
			String valorAEncriptar = verbo + path + expires + data;
			log.info("VALOR A ENCRIPTAR: {}", valorAEncriptar);
			String signature = Hex.encodeHexString(sha256_HMAC.doFinal((verbo + path + expires + data).getBytes("UTF-8")));
			log.info("SIGNATURE: {}", signature);
			return signature;
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
}
